/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wang.circleprogressview.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * TypedAttrUtils
 *
 * @since 2021-04-13
 */
public class TypedAttrUtils {
    /**
     * 日志常量
     */
    public static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TypedAttrUtils");

    /**
     * 颜色工具类
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return int
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getColorValue().getValue();
        } else {
            return defValue;
        }
    }

    /**
     * getColor
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return Color
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * getBoolean
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param isDefValue 默认值
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getBoolValue();
        } else {
            return isDefValue;
        }
    }

    /**
     * getString
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return String
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * getFloat
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return float
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * getFloat
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return float
     */
    public static int getInt(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * getDimensionPixelSize
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return int
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    public static float getDimension(AttrSet attrs, String attrName, float defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return int
     */
    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            HiLog.info(LABEL, "attr.getDimensionValue() = " + attrs.getAttr(attrName).get().getDimensionValue());
            return attrs.getAttr(attrName).get().getDimensionValue();
        } else {
            return defValue;
        }
    }

    /**
     * getElement
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @param defValue 默认值
     * @return Element
     */
    public static Element getElement(AttrSet attrs, String attrName, Element defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            HiLog.info(LABEL, "attr.getElement() = " + attrs.getAttr(attrName).get().getElement());
            return attrs.getAttr(attrName).get().getElement();
        } else {
            return defValue;
        }
    }

    /**
     * hasValue
     *
     * @param attrs 属性
     * @param attrName 属性名
     * @return boolean
     */
    public static boolean hasValue(AttrSet attrs, String attrName) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return true;
        } else {
            return false;
        }
    }
}
