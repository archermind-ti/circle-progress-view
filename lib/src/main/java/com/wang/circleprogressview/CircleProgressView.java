/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wang.circleprogressview;

import com.wang.circleprogressview.util.TypedAttrUtils;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class CircleProgressView extends Component implements Component.DrawTask, Component.EstimateSizeListener {


    /**
     * 画笔
     */
    private Paint mArcPaint;

    private Paint mPaint;

    /**
     * 文本画笔
     */
    private Paint mTextPaint;

    /**
     * 笔画描边的宽度
     */
    private float mStrokeWidth;

    /**
     * 开始角度(默认从12点钟方向开始)
     */
    private int mStartAngle = 270;

    /**
     * 扫描角度(一个圆)
     */
    private int mSweepAngle = 360;

    /**
     * 圆心坐标x
     */
    private float mCircleCenterX;

    /**
     * 圆心坐标y
     */
    private float mCircleCenterY;

    /**
     * 圆正常颜色
     */
    private static final int DEFAULT_NORMAL_COLOR = 0xFFC8C8C8;
    private Color mNormalColor = new Color(DEFAULT_NORMAL_COLOR);

    /**
     * 进度颜色
     */
    private static final int DEFAULT_PROGRESS_COLOR = 0xFF4FEAAC;
    private Color mProgressColor = new Color(DEFAULT_PROGRESS_COLOR);

    /**
     * 是否使用着色器
     */
    private boolean isShader = true;

    /**
     * 着色器
     */
    private Shader mShader;

    /**
     * 着色器颜色
     */
    private Color[] mShaderColors = new Color[]{new Color(0xFF4FEAAC), new Color(0xFFA8DD51), new Color(0xFFE8D30F), new Color(0xFFA8DD51), new Color(0xFF4FEAAC)};

    /**
     * 半径
     */
    private float mRadius;

    /**
     * 内圆与外圆的间距
     */
    private float mCirclePadding;

    /**
     * 刻度间隔的角度大小
     */
    private int mTickSplitAngle = 5;

    /**
     * 刻度的角度大小
     */
    private int mBlockAngle = 1;

    /**
     * 总刻度数
     */
    private int mTotalTickCount;

    /**
     * 最大进度
     */
    private int mMax = 1;

    /**
     * 当前进度
     */
    private float mProgress = 0;

    /**
     * 动画持续的时间
     */
    private int mDuration = 500;

    /**
     * 标签内容
     */
    private String mLabelText;

    /**
     * 字体大小
     */
    private int mLabelTextSize;

    /**
     * 字体颜色
     */
    private static final int DEFAULT_LABEL_TEXT_COLOR = 0xFF009688;
    private Color mLabelTextColor = new Color(DEFAULT_LABEL_TEXT_COLOR);


    /**
     * 进度百分比
     */
    private float mProgressPercent;

    /**
     * 是否显示标签文字
     */
    private boolean isShowLabel = true;
    /**
     * 是否默认显示百分比为标签文字
     */
    private boolean isShowPercentText = true;
    /**
     * 是否显示外边框刻度
     */
    private boolean isShowTick = true;

    /**
     * 是否旋转
     */
    private boolean isTurn = false;


    private boolean isMeasureCircle = false;

    private static final int NEARLY_FULL_CIRCLE = 350;

    private Component mComponent;

    private OnChangeListener mOnChangeListener;

    public CircleProgressView(Context context) {
        this(context, null);
    }

    public CircleProgressView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public CircleProgressView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mStrokeWidth = TypedAttrUtils.getDimension(attrSet, "strokeWith", 12);
        mLabelTextSize = TypedAttrUtils.getInt(attrSet, "labelTextSize", 50);
        mCirclePadding = TypedAttrUtils.getDimension(attrSet, "circlePadding", 10);

        isShader = false;

        mNormalColor = TypedAttrUtils.getColor(attrSet, "normalColor", mNormalColor);
        mProgressColor = TypedAttrUtils.getColor(attrSet, "progressColor", mProgressColor);

        mStartAngle = TypedAttrUtils.getInt(attrSet, "startAngle", 270);
        mSweepAngle = TypedAttrUtils.getInt(attrSet, "sweepAngle", 360);
        mMax = TypedAttrUtils.getInt(attrSet, "maxProgress", 1);
        mProgress = TypedAttrUtils.getInt(attrSet, "currentProgress", 0);

        mDuration = TypedAttrUtils.getInt(attrSet, "duration", 500);
        mLabelText = TypedAttrUtils.getString(attrSet, "labelText", null);
        mLabelTextColor = TypedAttrUtils.getColor(attrSet, "labelTextColor", mLabelTextColor);
        isShowLabel = TypedAttrUtils.getBoolean(attrSet, "isShowLabel", true);

        isShowTick = TypedAttrUtils.getBoolean(attrSet, "isShowTick", true);
        mTickSplitAngle = TypedAttrUtils.getInt(attrSet, "tickSplitAngle", mTickSplitAngle);
        mBlockAngle = TypedAttrUtils.getInt(attrSet, "blockAngle", mBlockAngle);
        isTurn = TypedAttrUtils.getBoolean(attrSet, "isTurn", isTurn);

        if (mLabelText == null) {
            isShowPercentText = true;
        } else {
            isShowPercentText = false;
        }
        initData();
        setEstimateSizeListener(this);
    }

    private void initData() {


        mPaint = new Paint();
        mArcPaint = new Paint();
        mTextPaint = new Paint();
        mProgressPercent = mProgress;
        mTotalTickCount = mSweepAngle / (mTickSplitAngle + mBlockAngle);

        addDrawTask(this);
    }

    /**
     * 设置进度改变监听
     *
     * @param onChangeListener
     */
    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.mOnChangeListener = onChangeListener;
    }


    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));

        mCircleCenterX = (width + getPaddingLeft() - getPaddingRight()) / 2.0f;
        mCircleCenterY = (height + getPaddingTop() - getPaddingBottom()) / 2.0f;

        //计算间距
        int padding = Math.max(getPaddingLeft() + getPaddingRight(), getPaddingTop() + getPaddingBottom());
        //半径=视图宽度-横向或纵向内间距值 - 画笔宽度
        mRadius = (width - padding - mStrokeWidth) / 2.0f - mCirclePadding;
        //默认着色器
        mShader = new SweepShader(mCircleCenterX, mCircleCenterX, mShaderColors, null);
        isMeasureCircle = true;
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        drawArc(canvas);
        drawOval(canvas);
        drawText(canvas);
    }


    private void drawArc(Canvas canvas) {
        mArcPaint.reset();
        mArcPaint.setAntiAlias(true);
        mArcPaint.setStyle(Paint.Style.STROKE_STYLE);
        mArcPaint.setStrokeWidth(mStrokeWidth);
        //先画出默认的(外边框刻度)
        float tickDiameter = mRadius * 2;
        float tickStartX = mCircleCenterX - mRadius;
        float tickStartY = mCircleCenterY - mRadius;
        RectFloat rectF = new RectFloat(tickStartX, tickStartY, tickStartX + tickDiameter, tickStartY + tickDiameter);

        for (int i = 0; i < mTotalTickCount; i++) {
            mArcPaint.setColor(mNormalColor);
            //绘制外边框刻度
            Arc arc = new Arc();
            arc.setArc(i * (mBlockAngle + mTickSplitAngle) + mStartAngle, mBlockAngle, false);
            canvas.drawArc(rectF, arc, mArcPaint);
        }

        if (mProgressPercent != 0) {
            final int currentBlockIndex = (int) (mProgressPercent * mTotalTickCount);
            for (int i = 0; i < currentBlockIndex; i++) {
                if (isShader && mShader != null) {
                    mArcPaint.setShader(mShader, Paint.ShaderType.SWEEP_SHADER);
                } else {
                    mArcPaint.setColor(mProgressColor);
                }
                //绘制外边框刻度
                Arc arc = new Arc();
                arc.setArc(i * (mBlockAngle + mTickSplitAngle) + mStartAngle, mBlockAngle, false);
                canvas.drawArc(rectF, arc, mArcPaint);
            }
        }

    }

    private void drawOval(Canvas canvas) {
        mPaint.reset();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(mStrokeWidth);
        //进度圆半径
        float circleRadius = isShowTick ? mRadius - mCirclePadding - mStrokeWidth : mRadius;
        float diameter = circleRadius * 2;
        float startX = mCircleCenterX - circleRadius;
        float startY = mCircleCenterY - circleRadius;
        RectFloat rectF1 = new RectFloat(startX, startY, startX + diameter, startY + diameter);

        if (mNormalColor != null) {
            mPaint.setColor(mNormalColor);
            //绘制底层弧形
            Arc arc = new Arc();
            arc.setArc(mStartAngle, mSweepAngle, false);
            canvas.drawArc(rectF1, arc, mPaint);
        }

        //着色器不为空则设置着色器，反之用纯色
        if (isShader && mShader != null) {
            mPaint.setShader(mShader, Paint.ShaderType.SWEEP_SHADER);
        } else {
            mPaint.setColor(mProgressColor);
        }

        Arc arc = new Arc();
        if (isTurn) {
            arc.setArc(mStartAngle + mSweepAngle * getRatio(), mSweepAngle * getRatio(), false);
        } else {
            arc.setArc(mStartAngle, mSweepAngle * getRatio(), false);
        }
        canvas.drawArc(rectF1, arc, mPaint);
    }

    /**
     * 绘制中间的文本
     *
     * @param canvas
     */
    private void drawText(Canvas canvas) {
        if (!isShowLabel) {
            return;
        }
        mTextPaint.reset();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setStyle(Paint.Style.FILL_STYLE);
        mTextPaint.setTextSize(mLabelTextSize);
        mTextPaint.setColor(mLabelTextColor);

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        // 计算文字高度 
        float fontHeight = fontMetrics.bottom - fontMetrics.top;

        // 计算文字baseline 
        float textBaseX = getWidth() / 2f;
        float textBaseY = getHeight() - (getHeight() - fontHeight) / 2f - fontMetrics.bottom;
        if (isShowPercentText) {//是否显示百分比
            String text = (int) (mProgressPercent * 100) + "%";
            float with = mTextPaint.measureText(text);
            canvas.drawText(mTextPaint, text, textBaseX - with / 2, textBaseY);
        } else if (mLabelText != null) {//显示自定义文本
            canvas.drawText(mTextPaint, mLabelText, textBaseX, textBaseY);
        }
    }

    /**
     * 显示进度动画效果
     */
    public void showAnimation(int duration, float progress, final boolean isAdd, Color[] colors, Color mProgressColor) {
        this.mDuration = duration;
        AnimatorValue valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(mDuration);
        valueAnimator.setLoopedCount(0);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (isAdd) {
                    if ((v <= progress)) {
                        setProgress(v, colors, mProgressColor);
                    } else {
                        setProgress(progress, colors, mProgressColor);
                    }
                } else {
                    if (1 - v >= progress) {
                        setProgress(progress, colors, mProgressColor);
                    } else {
                        setProgress(1 - v, colors, mProgressColor);
                    }
                }

            }
        });
        valueAnimator.start();
    }

    /**
     * 进度比例
     *
     * @return
     */
    private float getRatio() {
        return mProgress;
    }

    /**
     * 设置最大进度
     *
     * @param max
     */
    public void setMax(int max) {
        this.mMax = max;
        invalidate();
    }

    /**
     * 设置当前进度
     *
     * @param progress
     */
    public void setProgress(float progress, Color[] colors, Color progressColor) {
        this.mProgress = progress;
        mProgressPercent = mProgress;
        if (colors != null) {
            setProgressColor(mShaderColors);
        } else {
            setProgressColor(progressColor);
        }
        invalidate();
        if (mOnChangeListener != null) {
            mOnChangeListener.onProgressChanged(mProgress, mMax);
        }
    }

    public void setShader(Shader shader) {
        isShader = true;
        this.mShader = shader;
        invalidate();
    }

    /**
     * 设置进度颜色（通过着色器实现渐变色）
     *
     * @param colors
     */
    public void setProgressColor(Color[] colors) {
        isShader = true;
        if (isMeasureCircle) {
            Shader shader = new SweepShader(mCircleCenterX, mCircleCenterX, colors, null);
            setShader(shader);
        } else {
            mShaderColors = colors;
        }
    }

    /**
     * 设置进度颜色（纯色）
     *
     * @param color
     */
    public void setProgressColor(Color color) {
        isShader = false;
        this.mProgressColor = color;
    }

    /**
     * 设置是否显示外环刻度
     *
     * @param isShowTick
     */
    public void setShowTick(boolean isShowTick) {
        this.isShowTick = isShowTick;
        invalidate();
    }

    /**
     * 设置是否旋转
     *
     * @param isTurn
     */
    public void setTurn(boolean isTurn) {
        this.isTurn = isTurn;
        invalidate();
    }

    public interface OnChangeListener {
        void onProgressChanged(float progress, float max);
    }
}
