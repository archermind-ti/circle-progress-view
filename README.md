# CircleProgressView

## 简介
CircleProgressView是一个圆形渐变的进度动画控件（支持外环显示刻度，内环随之变化，配置参数完全可配），动画效果纵享丝滑

## 功能
圆形渐变的进度动画控件


## 演示
![演示Demo](./demo/demo.png)

## 集成

方式一

1. 下载[har](https://gitee.com/archermind-ti/circle-progress-view/releases/1.0.0-beta)包
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

```
  dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
     ……
  }
```

方式二

在project的build.gradle中添加mavenCentral()的引用

```
  repositories {   
 	...   
 	mavenCentral()   
	...           
  }
```

在entry的build.gradle中添加依赖

```
  dependencies { 
        ... 
        implementation 'com.gitee.archermind-ti:circleprogressview_ohos:1.0.0-beta' 
        ... 
  }
```



## 使用说明

1. 在xml中直接使用

```
    <com.wang.circleprogressview.CircleProgressBar
        ohos:id="$+id:circle_progress_bar"
        ohos:height="200vp"
        ohos:width="200vp"
        ohos:layout_alignment="horizontal_center"
        ohos:top_margin="20vp"/>
```
2. 在AbilitySlice中对其进行相关的设置

```
mCircleProgressBar.setMax(100);
```
3. 在点击事件中设置动画的效果参数为（动画时长，进度百分比，进度增加还是减少，变色的Color[]，进度条颜色Color）

```
//注意：Color[]和Color 只能选一个参数传值，代表是渐变色还是纯色
mCircleProgressBar.showAnimation(2000, 1f, true,mShaderColors,null);
```

## 编译说明
- 1.将项目通过git clone 至本地
- 2.使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
- 3.点击Run运行即可（真机运行可能需要配置签名）

## 版本迭代

- 1.0.0


## 版权和许可信息

  
The MIT License (MIT)

Copyright (c) 2017 Jenly Yu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
