/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wang.circleprogressview.slice;

import com.wang.circleprogressview.CircleProgressView;
import com.wang.circleprogressview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {

    private CircleProgressView mCircleProgressView;

    private Color[] mShaderColors = new Color[]{new Color(0xFF4FEAAC), new Color(0xFFA8DD51), new Color(0xFFE8D30F), new Color(0xFFA8DD51), new Color(0xFF4FEAAC)};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mCircleProgressView = (CircleProgressView) findComponentById(ResourceTable.Id_circle_progress_bar);
        mCircleProgressView.setMax(100);

        initCheckbox();
        initButton();
    }

    private void initCheckbox() {
        Checkbox checkbox1 = (Checkbox) findComponentById(ResourceTable.Id_check_box_one);
        checkbox1.setChecked(true);

        checkbox1.setCheckedStateChangedListener((absButton, b) -> {
            mCircleProgressView.setShowTick(b);
        });

        Checkbox checkbox2 = (Checkbox) findComponentById(ResourceTable.Id_check_box_two);
        checkbox2.setCheckedStateChangedListener((absButton, b) -> {
            mCircleProgressView.setTurn(b);
        });
    }

    private void initButton() {
        Button btnAdd = (Button) findComponentById(ResourceTable.Id_btnAdd);
        Button btnReduce = (Button) findComponentById(ResourceTable.Id_btnReduce);
        Button btnSingleAdd = (Button) findComponentById(ResourceTable.Id_btnSingleAdd);
        Button btnSingleReduce = (Button) findComponentById(ResourceTable.Id_btnSingleReduce);

        btnAdd.setClickedListener(component -> { ;
            mCircleProgressView.showAnimation(2000, 1f, true,mShaderColors,null);
        });

        btnReduce.setClickedListener(component -> {
            mCircleProgressView.showAnimation(2000, 1f, false,mShaderColors,null);
        });

        btnSingleAdd.setClickedListener(component -> {
            mCircleProgressView.showAnimation(2000,0.8f,true,null,new Color(0xFF4FEAAC));
        });

        btnSingleReduce.setClickedListener(component -> {
            mCircleProgressView.showAnimation(2000,0.8f,false,null,new Color(0xFF4FEAAC));
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
